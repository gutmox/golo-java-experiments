module helloWorld

# How to run it:
# golo golo --files hello-world.golo

function main = |args| {
  println("👋 Hello Golo")
}
