module simple.use

# import the Java package
import acme

function runBusterSample = {
  # constructor
  let buster = Toon("Buster") # no new
  # call an instance method `:` notation (colon)
  buster: hi()

  # use setter
  buster: nickName("busty")
  # use getter
  println(buster: nickName())

  return buster
}

function runElmiraSample = |buster| {
  # call a class method (static) `.` notation (dot)
  let elmira = Toon.getInstance("elmira")
  println(elmira: name())
  elmira: name("Elmira")
  println(elmira: name())
  elmira: hug(buster)
}

