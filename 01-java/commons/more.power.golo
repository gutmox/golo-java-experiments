module more.power

# import the Java package
import acme

augment acme.Toon {
  # override
  function hi = |this, message| {
    this: hi()
    println("👋 my message: " + message)
  }
  # new method
  function hello = |this| {
    println("👋 Hello 🌍 I'm " + this: name())
  }

}