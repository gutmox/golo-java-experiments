module more.more

import gololang.Adapters
import acme

# Finally we do not encourage you to use adapters as part of Golo code outside of providing bridges to third-party APIs.

function getExtendedToon = |name| {
  let toonAdapter = Adapter(): extends("acme.Toon")
    : implements("hi", |this| {
        println("HI!, I'M " + this: name())
      })
    : overrides("hug", |super, this, toon| {
        println("🟢 before")
        super(this, toon)
        println("🟢 after")
      })
    
  return toonAdapter: newInstance(name)
}
