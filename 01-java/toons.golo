#!/usr/bin/env golosh
module toons
# # golo golo --classpath libs/*.jar --files commons/*.golo toons.golo
# or
# chmod +x toons.golo
# ./toons.golo

import more.power
import simple.use
import more.more


function main = |args| {
  # simple.use module
  let buster = runBusterSample()
  runElmiraSample(buster)

  # more.power module
  buster: hi("what's up?")
  buster: hello()

  let babs = getExtendedToon("Babs")
  babs: hi()
  babs: hug(buster)

}
