FROM registry.gitlab.com/golo-lang/golo-image:latest

RUN apt-get install -y curl && \
    curl -sL https://deb.nodesource.com/setup_15.x | bash - && \
    apt-get install -y nodejs
