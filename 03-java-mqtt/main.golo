#!/usr/bin/env golosh
module main

import io.netty.handler.codec.mqtt.MqttQoS
import io.vertx.core.buffer.Buffer
import io.vertx.mqtt.MqttEndpoint
import io.vertx.core.Vertx
import io.vertx.mqtt.MqttServer
import io.vertx.mqtt.MqttServerOptions

import gololang.Errors

augment java.util.Map {
  @option
  function getOptional = |this, key| -> this: get(key)
}

function main = |args| {

  let vertx = Vertx.vertx()
  let mqtt_options = MqttServerOptions(): port(1883) # port(1883) / setPort
  let mqtt_server = MqttServer.create(vertx, mqtt_options)

  let clients=map[]
  let subscriptions=map[]

  mqtt_server: endpointHandler(|endpoint| {
    endpoint: accept(false)

    # update clients connection
    clients: put(endpoint: clientIdentifier(), endpoint)
    println("🤖 connected client: " + endpoint: clientIdentifier())

    endpoint: subscribeHandler(|subscribe| {
      # update clients subscriptions
      subscribe: topicSubscriptions(): each(|subscription| {
        subscriptions: put(
          endpoint: clientIdentifier() + "-" + subscription: topicName(),
          true
        )
      })
      println("😊 new subscriptions(s): " + subscribe: topicSubscriptions(): head())
    })
    # 🚧 TODO: unsubscribe

    endpoint: publishHandler(|message| {
      # You've got a 📬
      # for each 👨‍ check and dispatch messages
      clients: each(|identifier, client| {
        # if 👨‍ has subscribed to the current topic, then send 💌
        subscriptions: getOptional(identifier + "-" + message: topicName())
          : either(
              default= { # no subscription
                # nothing todo
              },
              mapping= |isSubscriptionActive| {
                if isSubscriptionActive is true { client: publish(
                    message: topicName()
                  , Buffer.buffer(message: payload(): toString())
                  , MqttQoS.AT_LEAST_ONCE()
                  , false
                  , false
                )}
              }
            )
      })
      println("📬 message on topic: " + message: topicName())
      println("👋 you've got a 📩: " + message: payload())
    })
    # 🚧 TODO: other events

  }) # end of mqtt_server: endpointHandler

  mqtt_server: listen()
  println("😄 listening on " + mqtt_server: actualPort())

}
