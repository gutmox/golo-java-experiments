#!/usr/bin/env golosh
module hello
# chmod +x hello.golo
# run: ./hello.golo

import mytools

# How to run it:
# golo golo --files hello-world.golo

function main = |args| {
  println("👋 Hello Golo")
  println(sayHello())
  println(sayHello("Bob"))
}
